export const getShortWalletAddress = (address) =>
  `${address.slice(0, 6)}...${address.slice(-4)}`;
