import {
  bUSDCContractABI,
  PriceProviderAggregatorABI,
  PrimaryIndexTokenABI,
} from "./abi";

export const PrimaryIndexToken = {
  address: process.env.REACT_APP_SC_PRIMARY_INDEX_TOKEN,
  abi: PrimaryIndexTokenABI,
};

export const bUSDCContract = {
  address: process.env.REACT_APP_SC_BUSDC_ADDRESS,
  abi: bUSDCContractABI,
};

export const PriceProviderAggregatorContract = {
  address: process.env.REACT_APP_PRICE_PROVIDER_AGGREGATOR,
  abi: PriceProviderAggregatorABI,
};

export const lendingTokenAddress = process.env.REACT_APP_LENDING_TOKEN_ADDRESS;
