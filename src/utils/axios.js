import axios from "axios";

const axiosInstance = axios.create({
  baseURL: process.env.REACT_APP_API_URL,
  headers: { "Content-Type": "application/json; charset=utf-8" },
});

axiosInstance.interceptors.response.use((response) => response.data);

export default axiosInstance;
