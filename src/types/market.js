import PropTypes from "prop-types";

export const linePropType = PropTypes.shape({
  title: PropTypes.string,
  color: PropTypes.string,
  value: PropTypes.string,
});
