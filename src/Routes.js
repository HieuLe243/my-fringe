import { Redirect, Route, Switch } from "react-router-dom";

import {
  BorrowerDashboard,
  LenderDashboard,
  IndexMarket,
  TermsOfService,
} from "pages";

const Routes = () => (
  <Switch>
    <Route exact path="/" component={BorrowerDashboard} />
    <Route exact path="/lend" component={LenderDashboard} />
    <Route exact path="/markets" component={IndexMarket} />
    <Route exact path="/terms-of-service" component={TermsOfService} />
    <Redirect to="/" />
  </Switch>
);

export default Routes;
