import { useQuery } from "react-query";

import { coinAPI } from "utils/api";
import ethereum from "utils/ethereum";

const useAvailableCoins = () =>
  useQuery("available-coins", async () => {
    const [{ data }] = await Promise.all([coinAPI.getCoins()]);

    const connected = Boolean(ethereum.account);

    if (connected) {
      const formattedData = data.map(async (item) => {
        const [balance, allowance, tokenPrice, healthFactor, lvr] =
          await Promise.all([
            ethereum.getBalance(item.address),
            ethereum.isTokenAllowance(item.address),
            ethereum.getPrice(item.address),
            ethereum.getHealthFactor(item.address),
            ethereum.getLVR(item.address),
          ]);

        return {
          ...item,
          balance,
          allowance,
          healthFactor,
          lvr,
          price: balance * tokenPrice,
        };
      });

      const newData = await Promise.all(formattedData);

      return newData;
    }

    return data;
  });

export default useAvailableCoins;
