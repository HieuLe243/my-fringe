import { useQuery } from "react-query";

import ethereum from "utils/ethereum";

const useAccountBalance = () =>
  useQuery("account-balance", async () => {
    const connected = Boolean(ethereum.account);

    if (connected) {
      const { balance, symbol } = await ethereum.getLendingTokenBalanceOf();

      return [{ balance, symbol }];
    }

    return [
      {
        balance: 0,
        symbol: "USDCTest",
      },
    ];
  });

export default useAccountBalance;
