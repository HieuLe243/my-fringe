import { useQuery } from "react-query";

import { borrowMarketAPI, coinAPI } from "utils/api";
import ethereum from "utils/ethereum";
import sumBy from "lodash/sumBy";

import { lendingTokenAddress } from "utils/ethereum/contracts";

const useActionsLog = () =>
  useQuery("deposited-borrowed", async () => {
    const [{ data }] = await Promise.all([coinAPI.getCoins()]);

    const connected = Boolean(ethereum.account);

    if (connected) {
      const { data: symbolCoinMap } = await borrowMarketAPI.getCoins(
        ethereum.account
      );

      const isLendingTokenAllowance = await ethereum.isLendingTokenAllowance(
        lendingTokenAddress
      );

      const formattedData = data.map(async (item, index) => {
        const [
          balance,
          allowance,
          tokenPrice,
          pitAmount,
          prjAmount,
          totalInIndex,
        ] = await Promise.all([
          ethereum.getBalance(item.address),
          ethereum.isTokenAllowance(item.address),
          ethereum.getPrice(item.address),
          ethereum.getPitAmount(`0x${item.address}`),
          ethereum.getPrjAmount(`0x${item.address}`),
          ethereum.getTotalInIndex(item.address),
        ]);

        const prjId = index;

        const healthFactor = symbolCoinMap[item.name]?.healthFactor?.rounded;
        const coinObject = symbolCoinMap[item.symbol];

        let collateralBalance;
        if (coinObject) {
          const [collateralBalanceAmountObject, symbol] =
            coinObject?.collateralBalance;
          const collateralBalanceAmount = Number(
            collateralBalanceAmountObject.decimal || "0"
          );

          const collateralBalanceAmountUsd =
            collateralBalanceAmount * tokenPrice;

          const amount = {
            rounded: collateralBalanceAmountUsd,
            decimal: collateralBalanceAmountUsd.toString(),
          };

          collateralBalance = [collateralBalanceAmount, symbol, amount];
          coinObject.collateralBalance = collateralBalance;
        }

        return {
          ...item,
          balance,
          allowance,
          healthFactor,
          price: balance * tokenPrice,
          tokenPrice,
          prjId,
          pitAmount,
          prjAmount,
          totalInIndex,
          lendingToken: { allowance: isLendingTokenAllowance },
          data: coinObject || {},
        };
      });

      const coins = await Promise.all(formattedData);

      const totalPitAmount = sumBy(coins, "pitAmount");

      const infoData = {
        depositedTotalBalance: sumBy(
          coins.map((coin) => {
            const balance = coin?.data?.collateralBalance;

            if (balance) {
              return balance[2].rounded;
            }

            return 0;
          })
        ),
        borrowingCapacity: sumBy(
          coins.map((coin) => coin?.data?.pitRemaining?.rounded || 0)
        ),
        totalOutstanding: sumBy(
          coins.map((coin) => coin?.data?.totalOutstanding?.rounded || 0)
        ),
        aggregatedAccruedInterest: sumBy(
          coins.map((coin) => coin?.data?.accrual?.rounded || 0)
        ),
      };

      return { coins, totalPitAmount, infoData };
    }

    return {
      coins: data,
      totalPitAmount: 0,
      infoData: {
        depositedTotalBalance: 0,
        borrowingCapacity: 0,
        totalOutstanding: 0,
        aggregatedAccruedInterest: 0,
      },
      lendingTokens: {},
    };
  });

export default useActionsLog;
