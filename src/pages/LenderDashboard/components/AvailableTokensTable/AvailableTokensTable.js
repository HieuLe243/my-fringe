import { useEffect, useMemo, useState } from "react";
import PropTypes from "prop-types";

import { lendingTokenPropType } from "types/lendingToken";

import { Box, Button, Paper } from "@material-ui/core";

import { useModalState } from "hooks";
import { CoinInfo, NumericText, Table, PaperTitle, Dialog } from "components";

import { SupplyModal } from "..";

const COLUMNS = [
  {
    Header: "Asset",
    accessor: "name",
    props: {
      width: 140,
    },
    Cell: ({
      row: {
        original: { symbol },
      },
    }) => (
      <CoinInfo logoUrl="./assets/coins_list/usd-coin.svg">{symbol}</CoinInfo>
    ),
  },
  {
    Header: "APY",
    accessor: "apy",
    props: {
      width: 100,
    },
    Cell: ({ value }) => <NumericText value={value} precision={2} suffix="%" />,
  },
  {
    Header: "Value",
    accessor: "balance",
    Cell: ({ value }) => <NumericText value={value} precision={2} />,
  },
];

const AvailableTokensTable = ({ data }) => {
  const { isOpen, onOpen, onClose } = useModalState();
  const [modalData, setModalData] = useState({});

  useEffect(() => {
    const activeToken = data.find(
      ({ address }) => address === modalData.address
    );
    if (activeToken) {
      setModalData(activeToken);
    }
  }, [data, modalData]);

  const columns = useMemo(
    () => [
      ...COLUMNS,
      {
        Header: "",
        accessor: "button",
        props: {
          align: "right",
          width: 150,
        },
        Cell: ({ row: { original } }) => (
          <Button
            fullWidth
            onClick={() => {
              setModalData(original);
              onOpen();
            }}
          >
            Deposit
          </Button>
        ),
      },
    ],
    [onOpen, setModalData]
  );

  return (
    <Paper>
      <PaperTitle>Available to supply</PaperTitle>
      <Box mt={2}>
        <Table columns={columns} data={data} />
      </Box>

      <Dialog open={isOpen} onClose={onClose} noPadding>
        <SupplyModal data={modalData} onClose={onClose} />
      </Dialog>
    </Paper>
  );
};

AvailableTokensTable.propTypes = {
  data: PropTypes.arrayOf(lendingTokenPropType).isRequired,
};

export default AvailableTokensTable;
