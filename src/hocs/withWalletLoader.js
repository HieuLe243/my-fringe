import { RINKEBY_TEST_NETWORK_CHAIN_ID } from "app/constants";
import { useWallet } from "hooks";

import Grid from "@material-ui/core/Grid";
import Alert from "@material-ui/lab/Alert";
import { makeStyles } from "@material-ui/core/styles";

import { Spinner } from "components";

const useStyles = makeStyles({
  alert: {
    justifyContent: "center",
  },
});

const withWalletLoader = (Component) => () => {
  const classes = useStyles();
  const { connecting, connected, chainId } = useWallet();

  if (connecting) {
    return <Spinner />;
  }

  if (connected && chainId !== RINKEBY_TEST_NETWORK_CHAIN_ID) {
    return (
      <Grid container justify="center">
        <Grid item md={4}>
          <Alert
            variant="filled"
            severity="error"
            icon={false}
            className={classes.alert}
          >
            Please choose the Rinkeby Test Network
          </Alert>
        </Grid>
      </Grid>
    );
  }

  return <Component />;
};

export default withWalletLoader;
