import { useEffect } from "react";
import { NavLink } from "react-router-dom";

import { makeStyles } from "@material-ui/core/styles";
import {
  AppBar,
  Toolbar,
  Button,
  Link,
  Hidden,
  Typography,
  Container,
} from "@material-ui/core";

import { ReactComponent as Logo } from "assets/svg/logo.svg";

import { useModalState, useWallet } from "hooks";

import { Dialog } from "components";
import { ConnectWalletDialog, MobileMenu } from "./components";

const useStyles = makeStyles((theme) => ({
  header: {
    backgroundColor: theme.palette.primary.main,
    borderBottom: "1px solid #2B2B2B",
  },
  appBar: {
    padding: "16px 24px",
    border: "none",
  },

  toolbar: {
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-start",
    padding: 0,
    [theme.breakpoints.up("lg")]: {
      flexDirection: "row",
      alignItems: "center",
    },
  },

  navigation: {
    flexGrow: 1,
    marginLeft: 40,
  },

  link: {
    paddingBottom: 3,
    borderBottom: "2px solid transparent",

    lineHeight: "20px",
    fontSize: 16,
    color: theme.palette.common.white,

    [theme.breakpoints.up("lg")]: {
      margin: theme.spacing(2),
    },

    "&:hover, &.active": {
      borderBottom: `2px solid ${theme.palette.secondary.main}`,
      boxSizing: "border-box",
    },
  },

  button: {
    width: "100%",
    maxWidth: "400px",
    marginTop: 30,
    marginLeft: "auto",
    marginRight: "auto",
    [theme.breakpoints.up("lg")]: {
      width: "auto",
      marginTop: 0,
    },
  },

  disabledButton: {
    "&.Mui-disabled": {
      backgroundColor: theme.palette.primary.light,
      color: theme.palette.common.white,
    },
  },

  logoLink: {
    display: "flex",
    alignItems: "center",
  },
  wallet: {
    color: "#515151",
    fontSize: 15,
    lineHeight: "19px",
    fontWeight: 500,
  },
  walletAddress: {
    color: theme.palette.success.light,
    fontWeight: 500,
  },
}));

const LINKS = [
  {
    path: "/",
    label: "Borrow",
    exact: true,
  },
  {
    path: "/lend",
    label: "Lend",
    exact: false,
  },
  {
    path: "/markets",
    label: "Markets",
    exact: false,
  },
];

const Header = () => {
  const classes = useStyles();
  const { account, status, Status } = useWallet();
  const { isOpen, onOpen, onClose } = useModalState();

  useEffect(() => {
    if (status === Status.walletExist) {
      onOpen();
    }
  }, [status, Status, onOpen]);

  return (
    <div className={classes.header}>
      <Container>
        <AppBar
          position="static"
          color="transparent"
          elevation={0}
          className={classes.appBar}
        >
          <Toolbar className={classes.toolbar}>
            <Link to="/" exact component={NavLink} className={classes.logoLink}>
              <Logo />
            </Link>
            <Hidden mdDown>
              <nav className={classes.navigation}>
                {LINKS.map(({ path, label, exact }) => (
                  <Link
                    key={path}
                    to={path}
                    exact={exact}
                    component={NavLink}
                    className={classes.link}
                    underline="none"
                  >
                    {label}
                  </Link>
                ))}
              </nav>
            </Hidden>

            <Hidden lgUp>
              <MobileMenu links={LINKS} />
            </Hidden>

            {account ? (
              <Typography className={classes.wallet}>
                Connected wallet:{" "}
                <Typography className={classes.walletAddress} component="span">
                  {account}
                </Typography>
              </Typography>
            ) : (
              <Button onClick={onOpen} className={classes.button}>
                Connect Wallet
              </Button>
            )}
          </Toolbar>

          <Dialog open={isOpen} onClose={onClose}>
            <ConnectWalletDialog onClose={onClose} />
          </Dialog>
        </AppBar>
      </Container>
    </div>
  );
};

export default Header;
