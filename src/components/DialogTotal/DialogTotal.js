import PropTypes from "prop-types";

import { makeStyles } from "@material-ui/core/styles";
import { Box, Grid, Typography } from "@material-ui/core";

import { NumericText } from "components";

const useStyles = makeStyles({
  dialogTotal: {
    width: "100%",
    backgroundColor: "#333333",
  },
});

const DialogTotal = ({ title, value, currency, children }) => {
  const classes = useStyles();
  return (
    <Box px={3} py={2} className={classes.dialogTotal}>
      {children || (
        <Grid container justify="space-between">
          <Grid item>
            <Typography color="secondary">{title}</Typography>
          </Grid>
          <Grid item>
            <Typography color="secondary">
              <NumericText value={value} suffix={currency} />
            </Typography>
          </Grid>
        </Grid>
      )}
    </Box>
  );
};

DialogTotal.propTypes = {
  title: PropTypes.string,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  currency: PropTypes.string,
  children: PropTypes.node,
};

DialogTotal.defaultProps = {
  title: "",
  value: 0,
  currency: "",
  children: null,
};

export default DialogTotal;
