export const TRANSACTION_TIMEOUT_MS = 10000; // 10s

export const RINKEBY_TEST_NETWORK_CHAIN_ID = "0x4";

export const DECIMAL_SCALE = 6;

export const CHART_COLORS = {
  total_pit: "#14A38B",
  total_amount_collateral_deposited: "#FF7171",
  total_outstanding: "#F2AC57",
  collateral_vs_loan_ration: "#A5A6F6",
  apy: "#EF5DA8",
};

export const EVENT_TYPES = {
  deposit: "Deposit",
  borrow: "Borrowed",
  withdraw: "Withdraw",
  repay: "Repay",
};
