const MuiCircularProgress = {
  props: {
    color: "primary",
  },
  overrides: {},
};

export default MuiCircularProgress;
