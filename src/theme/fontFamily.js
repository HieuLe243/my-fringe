import "@fontsource/ibm-plex-sans/400.css";
import "@fontsource/ibm-plex-sans/500.css";
import "@fontsource/ibm-plex-sans/600.css";
import "@fontsource/mulish";

const fontFamily = ["IBM Plex Sans", "Arial", "sans-serif"].join(",");

export default fontFamily;
