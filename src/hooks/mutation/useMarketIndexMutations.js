import { useCallback } from "react";
import { useQueryClient } from "react-query";

import { marketAPI } from "utils/api";

const useMarketIndexMutations = () => {
  const queryClient = useQueryClient();

  const getMarketIndex = useCallback(
    async (select, startAt) => {
      await queryClient.fetchQuery(
        ["market-index", { select, startAt }],
        async () => {
          const { data } = await marketAPI.getMarketIndex(select, startAt);
          return data;
        }
      );
    },
    [queryClient]
  );

  return {
    getMarketIndex,
  };
};

export default useMarketIndexMutations;
