import { useQuery } from "react-query";

import ethereum from "utils/ethereum";

const useBlockNumber = () => useQuery("block-number", ethereum.getBlockNumber);

export default useBlockNumber;
